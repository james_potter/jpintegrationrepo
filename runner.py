""" Executes all tests found within a glob search list """
from base.utils import *
import glob


class TestRunner:
    def __init__(self, test_path):
        self.path = test_path
        self.tests = glob.glob(test_path)
        self.tests_passed = 0
        self.tests_failed = 0
        xlogging(2, self.tests)

    @staticmethod
    def exec_file(filepath, globals=None, locals=None):
        if globals is None:
            globals = {}
        globals.update({
            "__file__": filepath,
            "__name__": "__main__",
        })
        with open(filepath, 'rb') as file:
            exec(compile(file.read(), filepath, 'exec'), globals, locals)

    def run(self):  # TODO: Concatenate incoming glob paths, important for handling multiple paths
        path_breakdown = []
        for i in self.tests:
            current_test = RecordTime(i)
            current_test.start()
            xlogging(2, f'running test {i}')
            try:
                self.exec_file(i)
                with open('results.txt', 'a') as f_result_pass:
                    f_result_pass.write(f"Test: {i} passed\n")
                self.tests_passed += 1
                xlogging(2, f"Total passed: {self.tests_passed} out of {len(self.tests)} tests")
            except Exception as e:
                prtsc_filename = i
                prtsc_filename = prtsc_filename.replace('.py', '')
                for file_path_section in prtsc_filename.split('\\'):
                    path_breakdown.append(file_path_section)
                prtsc_filename = f'FAILURE {path_breakdown[-3]} {path_breakdown[-1]}'
                xlogging(2, prtsc_filename)
                driver.execute_script("window.scroll(0, 0);")
                prtsc(prtsc_filename)
                xlogging(2, f'An error occurred during execution of test: {repr(e)})', 'y')
                with open('results.txt', 'a') as f_result_fail:
                    f_result_fail.write(f"\nTest: {i} failed\n")
                    f_result_fail.write(f"Error: {repr(e)}\n")
                    f_result_fail.write(f"Screenshot {prtsc_filename} saved to images\n\n")
                self.tests_failed += 1
            current_test.stop()
        xlogging(2, f"Total passed: {self.tests_passed} out of {len(self.tests)} tests")
        xlogging(2, f"Total failed: {self.tests_failed} out of {len(self.tests)} tests")
        if self.tests_failed != 0:
            with open('results.txt', 'a') as f_final_result_fail:
                f_final_result_fail.write(f'\n\nRun failed with {self.tests_failed} out of {len(self.tests)} tests failing')
            raise SystemExit(f'Run failed with {self.tests_failed} out of {len(self.tests)} tests failing')
        else:
            with open('results.txt', 'a') as f_final_result_pass:
                f_final_result_pass.write(f'\n\nRun finished with {self.tests_passed} out of {len(self.tests)} tests')
            xlogging(2, f'Run finished with {self.tests_passed} out of {len(self.tests)} tests')
            quit(0)

    def debug(self):
        for i in self.tests:
            current_test_raw = RecordTime(i)
            current_test_raw.start()
            xlogging(2, f'running test {i}')
            self.exec_file(i)
            current_test_raw.stop()
