from base import global_nav
from base.tools import *
from selenium.webdriver.common.action_chains import ActionChains


class Upgrade001Base:
    def __init__(self, content_name, url_suffix, component_label_value, component_upper_underscore):
        self.content_name = content_name
        self.url_suffix = url_suffix
        self.component_label_value = component_label_value
        self.component_upper_underscore = component_upper_underscore

    @staticmethod
    def edit_component_name(component):
        """ Types into the mandatory field for component title
        :param component: Uses the component label name from the drop down or from the side menu from layout
        :return: void """
        xlogging(2, f"Typing {date_time} {component} into component title.", log_as_step='y')
        try:
            xlogging(2, "Attempting initial edit of component title")
            wait_for_ajax_processes()
            web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-title-0-value'), 1).send_keys(f"{date_time} {component}")
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(2, "Could not find component title: Are you editing? Trying alternative path for inline editing")
            web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-title-0-value'), 3).send_keys(f"{date_time} {component}")

    @staticmethod
    def save_component():
        """ Clicks the add component button
        :return: void """
        xlogging(2, "Clicking create component", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-actions-ief-add-save')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-entities-0-actions-ief-entity-edit'))
        wait_for_ajax_processes()

    def add_content_page(self):
        """ Goes to content page
        :param content_name: Puts the whatever is passed down from content_name into text boxes etc. example: Banner
        :param url_suffix: The string required after .com/en/node/add/ <url_suffix> example:bricky
        :return: void """
        xlogging(2, f"Adding content page {self.content_name} by going to /en/node/add/{self.url_suffix}", log_as_step='y')
        driver.get(f"{EnvironmentSetup().get_base_url()}en/node/add/{self.url_suffix}")
        xlogging(1, "Typing: " + date_time + " " + self.content_name + ": into title field", log_as_step='y')
        web_wait("(" + xpath_start("input", "@class", "js-text-full text-full form-text required") + ")[1]").send_keys(date_time + " " + self.content_name)

    def bricky_select_component(self):
        """ Clicks the drop down menu for the body in the advanced edit page, selects the 'component' and
        clicks add component
        :return: void """
        configure_combobox('edit-field-body-wrapper', self.component_label_value)
        xlogging(2, "Clicking Add new component for 'Body'", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-actions-ief-add')).click()
        self.edit_component_name(self.component_label_value)

    def quick_edit_from_created_bricky(self):
        layout_path = web_wait(xpath_start('div', '@class', 'banner'))
        xlogging(2, "Hovering over banner", 1, log_as_step='y')
        ActionChains(driver).move_to_element(layout_path).perform()
        web_wait(xpath_start('button', '@class', 'trigger focusable'))

    def create_component_and_bricky(self):
        self.add_content_page()
        self.bricky_select_component()

    def save_component_and_bricky(self):
        self.save_component()
        global_nav.save_content_page(self.component_upper_underscore)
