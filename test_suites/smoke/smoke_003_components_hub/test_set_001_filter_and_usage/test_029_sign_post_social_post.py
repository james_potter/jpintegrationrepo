"""In this test (Test No.28) adding Sign Post - Social Post to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Social Post'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Social_Post',
                         component_label_value='Sign Post - Social Post')

    def mandatory_config(self):
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into display title field", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-sppost-display-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into display account name configured field", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-account-name-configured-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-channel-wrapper', 'Twitter')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_029_bricky_add_sign_post_social_post():
    TestRunner().runner()


test_029_bricky_add_sign_post_social_post()
