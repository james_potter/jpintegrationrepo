"""In this test (Test No.25) adding Sign Post - Key facts, Sign Post - News to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Key facts'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Key_Facts',
                         component_label_value='Sign Post - Key facts')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_025_bricky_add_sign_post_key_facts():
    TestRunner().runner()


test_025_bricky_add_sign_post_key_facts()
