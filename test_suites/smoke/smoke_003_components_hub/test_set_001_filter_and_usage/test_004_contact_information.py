"""In this test (Test No.4) adding Contact Information to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = ''
        component_label_value = 'Contact Information'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Contact_Information',
                         component_label_value='Contact Information')

    def mandatory_config(self):
        xlogging(2, f"Adding new component within {self.component_label_value}")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-contact-information-actions-ief-add')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-contact-information-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value} child component contact")
        get_image(2)
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-contact-information-form-0-actions-ief-add-save')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-contact-information-entities-0-actions-ief-entity-edit'))

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_004_bricky_add_contact_information():
    TestRunner().runner()


test_004_bricky_add_contact_information()
