"""In this test (Test No.32) adding Tabs to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Tabs'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Tabs',
                         component_label_value='Tabs')

    def mandatory_config(self):
        configure_combobox('edit-field-body-form-0-field-tabs-referencing-wrapper', 'Multiple dynamic tabs')
        xlogging(2, "Clicking add new component for option selected in drop down", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-tabs-referencing-actions-ief-add')).click()
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into child component title", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-tabs-referencing-form-0-title-0-value')).send_keys(f"Typing '{self.date_time} {self.component_label_value}' into child component title")
        configure_combobox('edit-field-body-form-0-field-tabs-referencing-form-0-field-tab-categories-wrapper', 'Brightcove account')
        configure_combobox('edit-field-body-form-0-field-tabs-referencing-form-0-field-tab-views-wrapper', 'Board of directors')
        configure_combobox('edit-field-body-form-0-field-tabs-referencing-form-0-field-tab-views-wrapper', 'List', box_no=2)
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-tabs-referencing-form-0-actions-ief-add-save')).click()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_033_bricky_add_tabs():
    TestRunner().runner()


test_033_bricky_add_tabs()
