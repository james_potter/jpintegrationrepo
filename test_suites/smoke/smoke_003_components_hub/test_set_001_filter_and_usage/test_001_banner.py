"""In this test (Test No.1) adding Banner to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base
from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'banner'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Banner',
                         component_label_value='Banner')

    def mandatory_config(self):
        xlogging(2, f"Getting image for {self.component_label_value}")
        get_image(1)  # Get Image function at top
        wait_for_ajax_processes()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_001_bricky_add_banner():
    TestRunner().runner()


test_001_bricky_add_banner()
