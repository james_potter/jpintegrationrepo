"""In this test (Test No.27) adding Sign Post - Quote to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Quote'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Quote',
                         component_label_value='Sign Post - Quote')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_028_bricky_add_sign_post_quote():
    TestRunner().runner()


test_028_bricky_add_sign_post_quote()
