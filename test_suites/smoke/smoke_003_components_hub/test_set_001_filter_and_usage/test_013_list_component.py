"""In this test (Test No.13) adding List Component to the bricky"""
from base.tools import *
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'List Component'
        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='List_Component',
                         component_label_value='List Component')

    def mandatory_config(self):
        configure_combobox('edit-field-body-form-0-field-views-list-wrapper', 'Events')
        configure_combobox('edit-field-body-form-0-field-views-list-wrapper', 'Events without description', box_no=2)

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_013_bricky_add_list_component():
    TestRunner().runner()


test_013_bricky_add_list_component()
