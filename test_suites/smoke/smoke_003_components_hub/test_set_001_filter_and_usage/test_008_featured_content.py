"""In this test (Test No.8) adding Featured content to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Featured content'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Featured_Content',
                         component_label_value='Featured content')

    def mandatory_config(self):
        get_image(1)

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_008_bricky_add_featured_content():
    TestRunner().runner()


test_008_bricky_add_featured_content()
