"""In this test (Test No.36) adding Video Gallery to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Video Gallery'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Video_Gallery',
                         component_label_value='Video Gallery')

    def mandatory_config(self):
        xlogging(2, f"Typing {self.date_time} {self.component_label_value} into child component title", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-inner-gallery-video-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-inner-gallery-video-form-0-field-vg-video-source-wrapper', 'YouTube')
        xlogging(2, "Entering important youtube video url: https://www.youtube.com/watch?v=dQw4w9WgXcQ", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-inner-gallery-video-form-0-field-inner-video-id-0-value')).send_keys('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
        xlogging(2, "Clicking create component for child component", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-inner-gallery-video-form-0-actions-ief-add-save')).click()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_037_bricky_add_video_gallery():
    TestRunner().runner()


test_037_bricky_add_video_gallery()
