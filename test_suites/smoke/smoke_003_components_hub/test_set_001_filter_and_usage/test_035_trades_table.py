"""In this test (Test No.34) adding Trades Table to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Trades Table'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Trades_Table',
                         component_label_value='Trades Table')

    def add_existing_component_in_layout_builder(self):
        xlogging(2, "Trade tables isn't searchable so I have overridden the Add_Existsing_Component_In_Layout_builder method in parent class", log_as_step='y')

    def mandatory_config(self):
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into auth key field", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-trades-table-auth-key-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-trades-table-insref-wrapper', '.3BQ2')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_035_bricky_add_trades_table():
    TestRunner().runner()


test_035_bricky_add_trades_table()
