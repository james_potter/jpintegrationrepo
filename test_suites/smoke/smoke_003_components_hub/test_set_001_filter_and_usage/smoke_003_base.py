""" Base class for checking that a component can be filtered to in components hub """
from base import global_nav
from base.xpath_tools import *
from global_base_classes.components.components_base import ComponentsBase


class Smoke003Base(ComponentsBase):
    def __init__(self, test_title, component_upper_underscore, component_label_value, date_time=new_date_time()):
        super().__init__(component_upper_underscore, component_label_value, date_time)
        self.usage_counter = 0
        start_step_counter(f'{test_title}')
        self.component_upper_underscore = component_upper_underscore
        self.component_label_value = component_label_value
        print(self.date_time)

    def beginning_of_test(self):
        global_nav.add_content_page(self.component_label_value, 'bricky', self.date_time)
        self.bricky_select_component()
        self.toggle_component_shared_status()

    def mandatory_config(self):
        xlogging(2, f"No mandatory config for {self.component_upper_underscore} component")

    def middle_of_test(self):
        pass

    def additional_config(self):
        xlogging(2, f"No Additional config for {self.component_label_value} component")

    def save_component_and_content(self):
        self.save_component()
        self.usage_counter += 1
        global_nav.save_content_page(f'{self.component_label_value} for {self.component_upper_underscore}')
        self.select_usage_counter(str(self.usage_counter))
        self.filter_component_component_type()
