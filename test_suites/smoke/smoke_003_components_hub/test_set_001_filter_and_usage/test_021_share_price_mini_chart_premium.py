"""In this test (Test No.21) adding Share Price Mini-Chart - Premium to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base

from base.tools import *


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Share Price Mini-Chart - Premium'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Share_Price_Mini_Chart_Premium',
                         component_label_value='Share Price Mini-Chart - Premium')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-auth-key-0-value')).send_keys(f"Auth key for {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-insref-wrapper', '.3BQ2')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_021_bricky_add_share_price_mini_chart_premium():
    TestRunner().runner()


test_021_bricky_add_share_price_mini_chart_premium()
