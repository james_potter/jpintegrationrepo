"""In this test (Test No.24) adding Sign Post - Image/Image with Rollover/Document Download to the bricky"""
from test_suites.smoke.smoke_003_components_hub.test_set_001_filter_and_usage.smoke_003_base import Smoke003Base


class TestRunner(Smoke003Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Image/Image with Rollover/Document Download'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Image_Image_with_Rollover_Document_Download',
                         component_label_value='Sign Post - Image/Image with Rollover/Document Download')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.save_component_and_content()


def test_024_bricky_add_sign_post_image_image_with_rollover_document_download():
    TestRunner().runner()


test_024_bricky_add_sign_post_image_image_with_rollover_document_download()
