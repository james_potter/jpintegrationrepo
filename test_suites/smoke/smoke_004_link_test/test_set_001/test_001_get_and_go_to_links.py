from base.tools import *


def test_self():
    driver.get(EnvironmentSetup().get_base_url())
    start_step_counter(f'Checking all URLS available in home page')
    DateTime().new_date_time()
    get_page_urls()
    go_to_scraped_urls()


test_self()
