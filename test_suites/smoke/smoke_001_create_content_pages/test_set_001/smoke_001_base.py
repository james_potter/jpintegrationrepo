""" Adds the content page bricky with the status of Draft
"""
from base import global_nav
from base.utils import *


class Smoke001Base:
    def __init__(self, test_title, content_name_upper_spaces, content_name_low_underscores, content_name_url_add_node_name, status_upper, status_low):
        start_step_counter(f'{test_title}')
        self.date_time = new_date_time()
        self.test_title = test_title
        self.content_name_upper_spaces = content_name_upper_spaces
        self.content_name_low_underscores = content_name_low_underscores
        self.content_name_url_add_node_name = content_name_url_add_node_name
        self.status_upper = status_upper
        self.status_low = status_low

    def add_content_page(self):
        """ Starts to add a content page by going to the add node url
        :param content_name_upper_spaces: Example: Financial Calendar
        :param content_name_url_add_node_name: Example: eventcontent
        :return: void """
        global_nav.add_content_page(f'{self.content_name_upper_spaces}', f'{self.content_name_url_add_node_name}', self.date_time)

    def mandatory_config(self):
        """ Override this function if content type requires mandatory fields when creating
        :param content_name_upper_spaces: Example: 'Financial Calendar'
        :return: void """
        xlogging(2, f"No mandatory config for required for {self.content_name_upper_spaces} content")

    def set_content_page_url_alias(self):
        """ Set url alias when creating content page
        :param status_low: examples: draft, review, published
        :param content_name_low_underscores: Example: financial_calendar
        :return: void """
        global_nav.set_content_page_url_alias(f'/auto_test_{self.content_name_low_underscores}/{self.status_low}')

    def set_content_release_state(self):
        """ Set the content release state at the bottom of the page
        :param status_upper: Use: Draft, Review, Published
        :return: void """
        global_nav.set_content_release_state(f'{self.status_upper}')

    def save_content_page(self):
        """ Saves the content page
        :param content_name_upper_spaces: Example: Financial Calendar
        :return: void """
        global_nav.save_content_page(f'{self.content_name_upper_spaces}')

    def close_saved_page_notif(self):
        """ Closes green box notifying the user that the page is saved"""
        web_wait(xpath_start('button', '@role', 'button')).click()

    def begining_of_test(self):
        """ A group of functions before test specific config/action - A little redundant in this suite, but for other test suites it is the standard"""
        self.add_content_page()

    def end_of_test(self):
        """ A group of functions after test specific config/actions
        :param content_name_upper_spaces: Example: Financial Calendar
        :param content_name_low_underscores: Example financial_calendar
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published
        :return: void """
        self.set_content_page_url_alias()
        self.set_content_release_state()
        self.save_content_page()
        self.close_saved_page_notif()
