""" Adding media library video content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base
from base.tools import *


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Media Library Video'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Media Library Video',
                         content_name_low_underscores='media_library_video',
                         content_name_url_add_node_name='media_library_video',
                         status_low=status_low, status_upper=status_upper)

    def mandatory_config(self):
        xlogging(2, f"Typing: '{self.date_time} {self.content_name_upper_spaces}' into component title field")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-video-0-inline-entity-form-title-0-value')).send_keys(f'{self.date_time} {self.content_name_upper_spaces}')

        configure_combobox('edit-field-video-0-inline-entity-form-field-vg-video-source-wrapper', 'YouTube')

        xlogging(2, 'Entering important youtube url into Video ID text box', log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-video-0-inline-entity-form-field-inner-video-id-0-value')).send_keys('https://www.youtube.com/watch?v=dQw4w9WgXcQ')

        get_image(2)

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_008_media_library_video():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_008_media_library_video()
