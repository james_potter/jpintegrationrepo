""" Adding disclaimer content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base
from base.utils import *


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Disclaimer'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Disclaimer',
                         content_name_low_underscores='disclaimer',
                         content_name_url_add_node_name='mid_disclaimer',
                         status_low=status_low, status_upper=status_upper)

    def mandatory_config(self):
        country = 'Albania'
        xlogging(2, "Entering decline page: Site Map")
        web_wait(xpath_start("div", "@class", "js-form-item form-item js-form-type-entity-autocomplete form-type-entity-autocomplete js-form-item-field-decline-page-0-target-id form-item-field-decline-page-0-target-id") + xpath_chainer("child", "input", "@class", "form-autocomplete form-text required ui-autocomplete-input")).send_keys("site map")

        xlogging(2, "Entering country select label")
        web_wait(xpath_start("div", "@class", "js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-field-country-select1-label-0-value form-item-field-country-select1-label-0-value") + xpath_chainer("child", "input", "@class", "js-text-full text-full form-text required")).send_keys(self.date_time + " " + self.content_name_upper_spaces)

        xlogging(2, "Clicking first country box")
        try:
            web_wait("(" + xpath_start("ul", "@class", "select2-selection__rendered") + xpath_chainer("child", "li", "@class", "select2-search select2-search--inline") + xpath_chainer("child", "input", "@class", "select2-search__field") + ")[1]", 1).click()
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(4, "'Display Countries 1' Form is not correctly formatted")
            prtsc("Display Countries 1 Error")
            xlogging(3, "Using backup selector")
            web_wait(xpath_start('select', '@data-drupal-selector', 'edit-field-display-countries-1') + xpath_chainer('child', 'option', 'text()', country)).click()
        xlogging(2, "Entering country: " + country)

        try:
            web_wait(xpath_start("ul", "@class", "select2-results__options") + xpath_chainer("child", "li", "text()", country), 1).click()
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(3, "Unable to click box to open countries menu")

        xlogging(2, "Clicking first country box")
        try:
            web_wait("(" + xpath_start("ul", "@class", "select2-selection__rendered") + xpath_chainer("child", "li", "@class", "select2-search select2-search--inline") + xpath_chainer("child", "input", "@class", "select2-search__field") + ")[4]", 1).click()
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(4, "'Display Countries 2' Form is not correctly formatted")
            prtsc("Display Countries 2 Error")
            xlogging(3, "Using backup selector")
            web_wait(xpath_start('select', '@data-drupal-selector', 'edit-field-display-countries-2') + xpath_chainer('child', 'option', 'text()', country)).click()

        xlogging(2, "Entering country: " + country)
        web_wait(xpath_start("ul", "@class", "select2-results__options") + xpath_chainer("child", "li", "text()", country)).click()

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_009_disclaimer():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_009_disclaimer()
