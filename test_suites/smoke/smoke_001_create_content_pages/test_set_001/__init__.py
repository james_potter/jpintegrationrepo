__all__ = ['smoke_001_base', 'test_001_bricky',
'test_002_event',
'test_003_financial_calendar',
'test_004_history_timeline',
'test_005_landing_page',
'test_006_Link_document_content',
'test_007_media_library',
'test_008_media_library_video',
'test_009_disclaimer',
'test_010_news',
'test_011_basic_page',
'test_012_people',
'test_013_presentation',
'test_014_result',
'test_015_webform',
]