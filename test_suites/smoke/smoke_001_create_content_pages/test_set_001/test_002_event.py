""" Adding event content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base
from base.utils import *


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Event'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Event',
                         content_name_low_underscores='event',
                         content_name_url_add_node_name='eventcontent',
                         status_low=status_low, status_upper=status_upper)

    def mandatory_config(self):
        xlogging(1, "Typing: " + self.date_time + " " + self.content_name_upper_spaces + ": into event description field", log_as_step='y')
        web_wait(xpath_start("div", "@class", "form-textarea-wrapper") + xpath_chainer("child", "textarea", "@class", "js-text-full text-full form-textarea required resize-vertical")).send_keys(self.date_time + " " + self.content_name_upper_spaces + ": event description field")

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_002_event():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_002_event()
