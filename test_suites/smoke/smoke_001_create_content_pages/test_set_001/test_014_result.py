""" Adding result content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base
from base.utils import *


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Result'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Result',
                         content_name_low_underscores='result',
                         content_name_url_add_node_name='result',
                         status_low=status_low, status_upper=status_upper)

    def mandatory_config(self):
        xlogging(2, "Typing into second 'Title'")
        web_wait(xpath_start("div", "@class", "js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-field-link-document-content-form-0-title-0-value form-item-field-link-document-content-form-0-title-0-value") + xpath_chainer("child", "input", "@class", "js-text-full text-full form-text required")).send_keys(self.date_time + " " + self.content_name_upper_spaces)

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_014_result():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_014_result()
