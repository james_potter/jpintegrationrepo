""" Adding financial calendar content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Financial Calendar'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Financial Calendar',
                         content_name_low_underscores='financial_calendar',
                         content_name_url_add_node_name='financial_calendar',
                         status_low=status_low, status_upper=status_upper)

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_003_financial_calendar():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_003_financial_calendar()
