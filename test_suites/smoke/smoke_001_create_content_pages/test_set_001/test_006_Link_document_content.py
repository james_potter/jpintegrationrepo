""" Adding link document content content pages with the status of draft, review, published
"""
from test_suites.smoke.smoke_001_create_content_pages.test_set_001.smoke_001_base import Smoke001Base
from base.utils import *


class TestRunner(Smoke001Base):
    def __init__(self, status_low, status_upper):
        """ Runs the test
        :param status_low: Example: draft, review, published
        :param status_upper: Example: Draft, Review, Published """

        content_name_upper_spaces = 'Link Document Content'

        super().__init__(test_title=f'Create content type {content_name_upper_spaces} with a status of {status_low}',
                         content_name_upper_spaces='Link Document Content',
                         content_name_low_underscores='link_document_content',
                         content_name_url_add_node_name='link_document_content',
                         status_low=status_low, status_upper=status_upper)

    def set_content_page_url_alias(self):
        xlogging(2, "Url alias is not available for 'Link Document Content' content type", log_as_step='y')

    def runner(self):
        self.begining_of_test()
        self.mandatory_config()
        self.end_of_test()


def test_006_link_document_content():
    TestRunner('draft', 'Draft').runner()
    TestRunner('review', 'Review').runner()
    TestRunner('published', 'Published').runner()


test_006_link_document_content()
