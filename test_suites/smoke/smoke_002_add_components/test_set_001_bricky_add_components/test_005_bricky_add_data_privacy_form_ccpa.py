"""In this test (Test No.5) adding Data Privacy Form - CCPA to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Data Privacy Form - CCPA'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Data_Privacy_Form_CCPA',
                         component_label_value='Data Privacy Form - CCPA')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-ccpa-auth-key-0-value')).send_keys(f"API key for {self.component_label_value}")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-ccpa-source-id-0-value')).send_keys('1')
        web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-0-field-ccpa-source-culture-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
        web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', 'Afar (Djibouti)')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-ccpa-captcha-site-key-0-value')).send_keys(f"Captcha site key for {self.component_label_value}")

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_005_bricky_add_data_privacy_form_ccpa():
    TestRunner().runner()


test_005_bricky_add_data_privacy_form_ccpa()
