"""In this test (Test No.12) adding Layout to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Layout'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Layout',
                         component_label_value='Layout')

    def add_existing_component_in_layout_builder(self):
        xlogging(2, "I think Layout isn't searchable so I have overridden the Add_Existsing_Component_In_Layout_builder method in parent class", log_as_step='y')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_012_bricky_add_layout():
    TestRunner().runner()


test_012_bricky_add_layout()
