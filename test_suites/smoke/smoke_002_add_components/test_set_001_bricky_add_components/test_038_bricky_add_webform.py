"""In this test (Test No.37) adding Webform to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Webform'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Webform',
                         component_label_value='Webform')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_038_bricky_add_webform():
    TestRunner().runner()


test_038_bricky_add_webform()
