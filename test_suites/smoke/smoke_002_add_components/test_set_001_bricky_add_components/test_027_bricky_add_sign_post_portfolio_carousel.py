"""In this test (Test No.26) adding Sign Post - Portfolio carousel to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Portfolio carousel'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Portfolio_Carousel',
                         component_label_value='Sign Post - Portfolio carousel')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_027_bricky_add_sign_post_portfolio_carousel():
    TestRunner().runner()


test_027_bricky_add_sign_post_portfolio_carousel()
