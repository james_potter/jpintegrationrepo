"""In this test (Test No.20) adding Share Price Mini-Chart - Essential to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Share Price Mini-Chart - Essential'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Share_Price_Mini_Chart_Essential',
                         component_label_value='Share Price Mini-Chart - Essential')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-auth-key-0-value')).send_keys(f"Auth key for {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-insref-wrapper', '.3BQ2')
        web_wait(xpath_start('details', '@data-drupal-selector', 'edit-field-body-widget-form-inline-entity-form-group-chart')).click()
        configure_combobox('edit-field-body-form-0-field-share-price-chart-type-wrapper', 'line')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_020_bricky_add_share_price_mini_chart_essential():
    TestRunner().runner()


test_020_bricky_add_share_price_mini_chart_essential()
