"""In this test (Test No.35) adding Video Banner to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Video Banner'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Video_Banner',
                         component_label_value='Video Banner')

    def mandatory_config(self):
        xlogging(2, f"Typing {self.date_time} {self.component_label_value} into child component title", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-vbanner-front-video-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-body-form-0-field-vbanner-front-video-form-0-field-vg-video-source-wrapper', 'YouTube')
        xlogging(2, "Entering important youtube video url: https://www.youtube.com/watch?v=dQw4w9WgXcQ", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-vbanner-front-video-form-0-field-inner-video-id-0-value')).send_keys('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
        xlogging(2, "Clicking create component for child component", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-vbanner-front-video-form-0-actions-ief-add-save')).click()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_036_bricky_add_video_banner():
    TestRunner().runner()


test_036_bricky_add_video_banner()
