""" Adds the content page bricky with the status of Draft
"""
from base import global_nav
from base.tools import *
from selenium.webdriver.common.action_chains import ActionChains
from base.xpath_tools import *
from global_base_classes.components.components_base import ComponentsBase


class Smoke002Base(ComponentsBase):
    def __init__(self, test_title, component_upper_underscore, component_label_value):
        super().__init__(component_upper_underscore, component_label_value, date_time=new_date_time())
        self.date_time = new_date_time()
        start_step_counter(f'{test_title}')
        self.component_upper_underscore = component_upper_underscore
        self.component_label_value = component_label_value

    def beginning_of_test(self):
        global_nav.add_content_page(self.component_label_value, 'bricky', self.date_time)
        self.bricky_select_component()

    def mandatory_config(self):
        xlogging(2, f"No mandatory config for {self.component_upper_underscore} component")

    def middle_of_test(self):
        self.toggle_component_shared_status()

    def additional_config(self):
        xlogging(2, f"No Additional config for {self.component_label_value} component")

    def end_of_test(self):
        self.save_component()
        global_nav.save_content_page(self.component_upper_underscore)
        self.assert_page_name()
        self.go_to_advanced_edit()
        self.change_component_name()
        global_nav.set_content_release_state('Draft')
        global_nav.save_content_page('bricky')
        self.go_to_edit_layout_of_created_page()
        self.add_existing_component_in_layout_builder()
        global_nav.save_layout()
