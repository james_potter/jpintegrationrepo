"""In this test (Test No.10) adding Iframe to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Iframe'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Iframe',
                         component_label_value='Iframe')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-iframe-id-0-value')).send_keys(f"Iframe ID for {self.component_label_value}")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-iframe-url-0-uri')).send_keys(f"http://Iframe url for {self.component_label_value}")

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_010_bricky_add_iframe():
    TestRunner().runner()


test_010_bricky_add_iframe()
