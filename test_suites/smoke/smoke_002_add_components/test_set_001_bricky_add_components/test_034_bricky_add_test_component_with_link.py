"""In this test (Test No.33) adding Test component with Link to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Test component with Link'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Test_Component_With_Link',
                         component_label_value='Test component with Link')

    def runner(self):
        xlogging(2, "Component currently not available", log_as_step='y')
        # self.beginning_of_test()
        # self.mandatory_config()
        # self.middle_of_test()
        # self.additional_config()
        # self.end_of_test()


def test_034_bricky_add_test_component_with_link():
    TestRunner().runner()


test_034_bricky_add_test_component_with_link()
