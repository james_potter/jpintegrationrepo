"""In this test (Test No.14) adding Location list to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Location list'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Location_List',
                         component_label_value='Location list')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_014_bricky_add_location_list():
    TestRunner().runner()


test_014_bricky_add_location_list()
