"""In this test (Test No.22) adding Sign Post - Email Signup to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Email Signup'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Email_Signup',
                         component_label_value='Sign Post - Email Signup')

    def mandatory_config(self):
        xlogging(2, "Typing / to satisfy Redirect URL", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-sp-email-up-frame-url-0-uri')).send_keys('/')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_022_bricky_add_sign_post_email_signup():
    TestRunner().runner()


test_022_bricky_add_sign_post_email_signup()
