"""In this test (Test No.23) adding Sign Post - Event to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Event'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Event',
                         component_label_value='Sign Post - Event')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_023_bricky_add_sign_post_event():
    TestRunner().runner()


test_023_bricky_add_sign_post_event()
