"""In this test (Test No.3) adding Configurable Text to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base
from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = ''
        component_label_value = 'Configurable Text'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Configurable_Text',
                         component_label_value='Configurable Text')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_003_bricky_add_configurable_text():
    TestRunner().runner()


test_003_bricky_add_configurable_text()
