"""In this test (Test No.2) adding Configurable Image to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_001_bricky_add_components.smoke_002_bricky_base import Smoke002Base
from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Configurable Image'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Configurable_Image',
                         component_label_value='Configurable Image')

    def mandatory_config(self):
        xlogging(2, f'Getting image for {self.component_label_value}', log_as_step='y')
        get_image(1)  # Get Image function at top

    def additional_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-field-image-0-remove-button')).click()
        xlogging(2, f'Getting a new image for {self.component_label_value}', log_as_step='y')
        get_image(2)  # Get Image function at top

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_002_bricky_add_configurable_image():
    TestRunner().runner()


test_002_bricky_add_configurable_image()
