"""In this test (Test No.30) adding Signpost Map to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Signpost Map'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Signpost_Map',
                         component_label_value='Signpost Map')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_031_bricky_add_signpost_map():
    TestRunner().runner()


test_031_bricky_add_signpost_map()
