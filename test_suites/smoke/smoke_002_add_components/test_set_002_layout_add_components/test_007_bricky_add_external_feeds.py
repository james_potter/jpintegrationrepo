"""In this test (Test No.7) adding External feeds to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'External feeds'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='External_Feeds',
                         component_label_value='External feeds')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_007_bricky_add_external_feeds():
    TestRunner().runner()


test_007_bricky_add_external_feeds()
