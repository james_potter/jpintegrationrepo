"""In this test (Test No.6) adding Dividend Center to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Dividend Center'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Dividend_Center',
                         component_label_value='Dividend Center')

    # def save_component(self):
    #     """ Clicks the add component button
    #     :return: void """
    #     xlogging(2, f"Clicking create component for {self.component_label_value}", 'y')
    #     web_wait(xpath_start('input', '@data-drupal-selector', 'bricky-edit-submit-btn-modal')).click()
    #     wait_for_ajax_processes()
    #     wait_for_element_to_disappear(xpath_start('div', '@class', 'ui-widget-overlay ui-front'))
    #     xlogging(2, "Saving content page", 'y')
    #     web_wait(xpath_start('input', '@data-drupal-selector', 'bricky-edit-submit-btn')).click()

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-dvc-auth-key-0-value')).send_keys(f"Auth key for {self.component_label_value}")
        web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-dvc-insref-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
        web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', '.3BQ2')).click()

        web_wait(xpath_start('details', '@data-drupal-selector', 'edit-group-dividend-chart')).click()
        web_wait(xpath_start('details', '@data-drupal-selector', 'edit-group-dividend-history')).click()
        web_wait(xpath_start('details', '@data-drupal-selector', 'edit-group-dividend-calculator')).click()

        get_image(1)

        web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-dvc-default-time-tab-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
        web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', '5 Years')).click()

        random_hex_colour('edit-field-dvc-time-tab-theme-color-0-value')
        random_hex_colour('edit-field-dvc-annual-color-0-value')
        random_hex_colour('edit-field-dvc-half-year-color-0-value')
        random_hex_colour('edit-field-dvc-quarterly-color-0-value')
        random_hex_colour('edit-field-dvc-monthly-color-0-value')
        random_hex_colour('edit-field-dvc-bonus-color-0-value')
        random_hex_colour('edit-field-dvc-dividend-yield-colour-0-value')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_006_bricky_add_dividend_center():
    TestRunner().runner()


test_006_bricky_add_dividend_center()
