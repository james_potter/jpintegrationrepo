"""In this test (Test No.13) adding List Component to the bricky"""
from base.tools import *
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'List Component'
        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='List_Component',
                         component_label_value='List Component')

    def mandatory_config(self):
        configure_combobox('edit-field-views-list-wrapper', 'Events')
        configure_combobox('edit-field-views-list-wrapper', 'Events without description', box_no=2)

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_013_bricky_add_list_component():
    TestRunner().runner()


test_013_bricky_add_list_component()
