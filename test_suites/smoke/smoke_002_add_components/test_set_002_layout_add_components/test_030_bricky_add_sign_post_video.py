"""In this test (Test No.29) adding Sign Post - Video to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Video'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Video',
                         component_label_value='Sign Post - Video')

    def mandatory_config(self):
        xlogging(2, f"Typing {self.date_time} {self.component_label_value}' into second component title", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-video-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-video-form-0-field-vg-video-source-wrapper', 'YouTube')
        xlogging(2, "Entering important youtube video url: https://www.youtube.com/watch?v=dQw4w9WgXcQ", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-video-form-0-field-inner-video-id-0-value')).send_keys('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
        get_image(1)
        xlogging(2, "Saving child component", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-video-form-0-actions-ief-add-save')).click()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_030_bricky_add_sign_post_video():
    TestRunner().runner()


test_030_bricky_add_sign_post_video()
