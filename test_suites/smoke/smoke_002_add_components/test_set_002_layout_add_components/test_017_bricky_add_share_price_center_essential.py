"""In this test (Test No.17) adding Share Price Center - Essential to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Share Price Center - Essential'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Share_Price_Center_Essential',
                         component_label_value='Share Price Center - Essential')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-auth-key-0-value')).send_keys(f"Auth key for {self.component_label_value}")
        configure_combobox('edit-field-insref-wrapper', '.3BQ2')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_017_bricky_add_share_price_center_essential():
    TestRunner().runner()


test_017_bricky_add_share_price_center_essential()
