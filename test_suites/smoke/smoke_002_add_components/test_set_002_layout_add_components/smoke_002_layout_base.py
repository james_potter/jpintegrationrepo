""" Adds the content page bricky with the status of Draft
"""
from base import global_nav
from base.tools import *
from selenium.webdriver.common.action_chains import ActionChains
from base.xpath_tools import *
from global_base_classes.components.components_base import ComponentsBase


class Smoke002Base(ComponentsBase):
    def __init__(self, test_title, component_upper_underscore, component_label_value):
        super().__init__(component_upper_underscore, component_label_value, date_time=new_date_time())
        self.date_time = new_date_time()
        start_step_counter(f'{test_title}')
        self.component_upper_underscore = component_upper_underscore
        self.component_label_value = component_label_value

    @staticmethod
    def toggle_component_shared_status():
        xlogging(2, "Clicking share checkbox", 'y')
        web_wait(xpath_start('div', '@data-drupal-selector', 'edit-shared-status-wrapper') + xpath_chainer('descendant', 'span', '@class', 'icon icon-size-20')).click()

    def save_component(self):
        """ Clicks the add component button
        :return: void """
        xlogging(2, f"Clicking create component for {self.component_label_value}", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit')).click()
        wait_for_ajax_processes()
        wait_for_element_to_disappear(xpath_start('div', '@class', 'ui-widget-overlay ui-front'))
        xlogging(2, "Saving content page", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'bricky-edit-submit-btn')).click()

    def beginning_of_test(self):
        global_nav.add_content_page(self.component_label_value, 'bricky', self.date_time)
        global_nav.save_content_page(self.component_upper_underscore)
        self.go_to_edit_layout_of_created_page()
        self.add_new_component_from_layout_builder()

    def mandatory_config(self):
        xlogging(2, f"No mandatory config for {self.component_upper_underscore} component")

    def middle_of_test(self):
        self.toggle_component_shared_status()

    def additional_config(self):
        xlogging(2, f"No Additional config for {self.component_label_value} component")

    def end_of_test(self):
        self.save_component()
