"""In this test (Test No.9) adding History to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'History'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='History',
                         component_label_value='History')

    def mandatory_config(self):
        web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-history-views-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
        web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', 'ID History')).click()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_009_bricky_add_history():
    TestRunner().runner()


test_009_bricky_add_history()
