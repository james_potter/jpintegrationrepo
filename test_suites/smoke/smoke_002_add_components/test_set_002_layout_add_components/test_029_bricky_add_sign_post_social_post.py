"""In this test (Test No.28) adding Sign Post - Social Post to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Sign Post - Social Post'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Sign_Post_Social_Post',
                         component_label_value='Sign Post - Social Post')

    def mandatory_config(self):
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into display title field", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-sppost-display-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into display account name configured field", log_as_step='y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-account-name-configured-0-value')).send_keys(f"{self.date_time} {self.component_label_value}")
        configure_combobox('edit-field-channel-wrapper', 'Twitter')

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_029_bricky_add_sign_post_social_post():
    TestRunner().runner()


test_029_bricky_add_sign_post_social_post()
