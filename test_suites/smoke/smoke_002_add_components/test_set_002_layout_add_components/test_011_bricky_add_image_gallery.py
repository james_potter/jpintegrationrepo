"""In this test (Test No.11) adding Image Gallery to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = ''

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Image_Gallery',
                         component_label_value='Image Gallery')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-gallery-slide-actions-ief-add')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-gallery-slide-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value} child component gallery slide")
        get_image(1)
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-gallery-slide-form-0-actions-ief-add-save')).click()
        wait_for_ajax_processes()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_011_bricky_add_image_gallery():
    TestRunner().runner()


test_011_bricky_add_image_gallery()
