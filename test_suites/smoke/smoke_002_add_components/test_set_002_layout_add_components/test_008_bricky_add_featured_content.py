"""In this test (Test No.8) adding Featured content to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Featured content'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Featured_Content',
                         component_label_value='Featured content')

    def mandatory_config(self):
        get_image(1)

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_008_bricky_add_featured_content():
    TestRunner().runner()


test_008_bricky_add_featured_content()
