"""In this test (Test No.4) adding Contact Information to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = ''
        component_label_value = 'Contact Information'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Contact_Information',
                         component_label_value='Contact Information')

    # def save_component(self):
    #     """ Clicks the add component button
    #     :return: void """
    #     xlogging(2, f"Clicking create component for {self.component_label_value}", 'y')
    #     web_wait(xpath_start('input', '@data-drupal-selector', 'bricky-edit-submit-btn-modal')).click()
    #     wait_for_ajax_processes()
    #     wait_for_element_to_disappear(xpath_start('div', '@class', 'ui-widget-overlay ui-front'))
    #     xlogging(2, "Saving content page", 'y')
    #     web_wait(xpath_start('input', '@data-drupal-selector', 'bricky-edit-submit-btn')).click()

    def mandatory_config(self):
        xlogging(2, f"Adding new component within {self.component_label_value}")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-contact-information-actions-ief-add')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-contact-information-form-0-title-0-value')).send_keys(f"{self.date_time} {self.component_label_value} child component contact")
        get_image(2)
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-contact-information-form-0-actions-ief-add-save')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-contact-information-entities-0-actions-ief-entity-edit'))

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_004_bricky_add_contact_information():
    TestRunner().runner()


test_004_bricky_add_contact_information()
