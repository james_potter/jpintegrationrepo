"""In this test (Test No.1) adding Banner to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base
from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'banner'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Banner',
                         component_label_value='Banner')

    def mandatory_config(self):
        xlogging(2, f"Getting image for {self.component_label_value}")
        get_image(1)
        wait_for_ajax_processes()

    def additional_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-banner-image-0-remove-button')).click()
        wait_for_ajax_processes()
        xlogging(2, f"Getting a new image for {self.component_label_value}")
        get_image(2)
        wait_for_ajax_processes()

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_001_bricky_add_banner():
    TestRunner().runner()


test_001_bricky_add_banner()
