"""In this test (Test No.19) adding Share Price Mini-Chart - Elite to the bricky"""
from test_suites.smoke.smoke_002_add_components.test_set_002_layout_add_components.smoke_002_layout_base import Smoke002Base

from base.tools import *


class TestRunner(Smoke002Base):
    def __init__(self):
        """ Runs the test """

        component_label_value = 'Share Price Mini-Chart - Elite'

        super().__init__(test_title=f'Create content type {component_label_value}',
                         component_upper_underscore='Share_Price_Mini_Chart_Elite',
                         component_label_value='Share Price Mini-Chart - Elite')

    def mandatory_config(self):
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-auth-key-0-value')).send_keys(f"Auth key for {self.component_label_value}")

    def runner(self):
        self.beginning_of_test()
        self.mandatory_config()
        self.middle_of_test()
        self.additional_config()
        self.end_of_test()


def test_019_bricky_add_share_price_mini_chart_elite():
    TestRunner().runner()


test_019_bricky_add_share_price_mini_chart_elite()
