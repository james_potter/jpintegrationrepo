import os


def get_image_path(image_name):
    return os.path.join(os.path.dirname(__file__), image_name)
