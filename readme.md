# Instructions / Manual

## Setup

## Configuration

### Pass arguments:

* Arg 1) - os: win, lin, mac
* Arg 2) - browser: Firefox, Chrome
* Arg 3) - base url: https://www.url.com/
    * It's important to have the '/' at the end of the url
* Arg 4) - username: username
* Arg 5) - password: password
* Arg 6) - Test list: system base path(i.e. c:\path\to\repo) and '\test_suites' are covered in code.
    * Format: \path\to\test.py,\path2\to2\test2.py,\path3\to3\test3.py
    * Please be aware that * can be used for example: \smoke\*\\\*\test_* would return all tests within the smoke directory

* If arguments are not passed, the program will try to get the settings from the setup.yml file
* If the setup.yml has empty or invalid arguments then the program will ask for user input.

Example: python main.py lin Firefox https://www.url.com/ username password \path\to\test*,\another\test\,\ano\*\test,\another\\*t*

## Conventions:

### Naming Formats:

* file_name
* ClassName
* function_name
* variable_name