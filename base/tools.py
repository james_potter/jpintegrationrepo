from selenium.common.exceptions import TimeoutException

from .utils import *
from .setup import *
from base import window_switcher
import re

get_page_urls = []


def scrape_page_urls(tag='a'):
    """  Gets all hrefs within a tags from current
    :param tag: html tag to search for hrefs in default is the 'a' tag
    :return: void """
    global get_page_urls
    for i in driver.find_elements_by_xpath(f'.//{tag}'):
        get_page_urls.append(i.get_attribute('href'))
    xlogging(2, f"Found {len(get_page_urls)} hrefs inside the {tag} tag")

    filter_counter = 0

    for i in get_page_urls:
        xlogging(2, f"Unfiltered url: {str(i)}")
        with open('scraped_urls.txt', 'a') as file:
            file.write(f'Unfiltered url: {i}\n')

        if re.search('mailto|twitter|facebook|linkedin|logout|/flush/|/update.php|run-cron', str(i)):
            xlogging(2, f"Found a filter match: REMOVING: {i}")
            get_page_urls.remove(i)
            filter_counter += 1
        else:
            xlogging(2, f"{i} DID NOT MATCH FILTER")

    xlogging(2, f"Removed {filter_counter} urls")


def go_to_scraped_urls():
    """ To go the URLs collected from scrape_page_urls()
    :return: void """
    for a in get_page_urls:
        with open('scraped_urls.txt', 'w') as f_urls:
            f_urls.write(f'Filtered: {a}')

        try:
            xlogging(2, f"Going to: {a}")
            driver.get(a)
            if web_wait(xpath_start('body', 'text()', 'The website encountered an unexpected error. Please try again later.'), 0.1).text == 'The website encountered an unexpected error. Please try again later.':
                xlogging(4, f"Website encountered an error at url: {driver.current_url}", 'y')
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(2, f"Ivalid url: {a}")
            xlogging(2, "Error: URL is invalid, ignoring")

        try:
            a = a.replace(EnvironmentSetup().get_base_url(), "")
            a = a.replace(EnvironmentSetup().get_base_url(), "")
            a = a.replace("/", " ")
            xlogging(2, "Saving File: " + a + ".png")
            prtsc(f"Link_Pictures_{a}")  # call prtsc function and insert filename
        except Exception as e:
            xlogging(2, repr(e))
            xlogging(4, "Error: Likely that the URL was empty or invalid, continuing")


def clean_automation(page_type):
    """ Deleted content pages or components that start with the string 'auto:'
    :param page_type: Choose: 'content' or 'components'
    :return: void """
    if page_type == 'content':
        url_extension = 'en/admin/content?title=auto&type=All&langcode=All'
        xlogging(1, f"Concatenating {EnvironmentSetup().get_base_url()} with {url_extension}")
        driver.get(EnvironmentSetup().get_base_url() + url_extension)
        try:
            while True:
                xlogging(2, "Clicking combobox option for delete")
                configure_combobox('edit-node-bulk-form', 'Delete content')

                xlogging(2, "Clicking checkbox to select all results")
                web_wait(xpath_start('input', '@title', 'Select all rows in this table'), 1).click()

                xlogging(2, "Clicking Apply to selectd items button")
                web_wait(xpath_start('div', '@data-drupal-selector', 'edit-node-bulk-form') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit')).click()

                xlogging(2, f"Clicking {web_wait(xpath_start('form', '@data-drupal-selector', 'node-delete-multiple-confirm-form') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit')).get_attribute('value')}")
                web_wait(xpath_start('form', '@data-drupal-selector', 'node-delete-multiple-confirm-form') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit')).click()

                xlogging(2, "Ensuring 'auto' is present as the search value")
                assert web_wait(xpath_start('form', '@data-drupal-selector', 'views-exposed-form-content-page-1') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-title'), 30).get_attribute('value') == 'auto'

                xlogging(2, "Clicking filter to ensure results are automation related")
                web_wait(xpath_start('form', '@data-drupal-selector', 'views-exposed-form-content-page-1') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit-content')).click()
        except AssertionError:
            xlogging(2, "auto is not present as a search value, stopping clean up")
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(2, "Cannot find more results for 'auto' continuing")

    elif page_type == 'components':
        url_extension = 'en/admin/content/brick?title=auto&type=All&langcode=All'
        xlogging(1, f"Concatenating {EnvironmentSetup().get_base_url()} with {url_extension}")
        driver.get(EnvironmentSetup().get_base_url() + url_extension)
        try:
            while True:
                select_all_checkbox_state = web_wait(xpath_start('th', '@class', 'select-all views-field views-field-views-bulk-operations-bulk-form') + xpath_chainer('child', 'input', '@type', 'checkbox')).get_attribute('title')
                if select_all_checkbox_state == 'Select all rows in this table':
                    web_wait(xpath_start('input', '@title', 'Select all rows in this table')).click()
                web_wait(f"({xpath_start('div', '@data-drupal-selector', 'edit-actions')}{xpath_chainer('child', 'input', '@data-drupal-selector', 'edit-submit')})[1]").click()
                web_wait(xpath_start('form', '@data-drupal-selector', 'views-bulk-operations-confirm-action') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit')).click()
                web_wait(xpath_start('h1', 'text()', 'Component'), 30)
                assert driver.current_url == "https://connectid-profile-v2.qid1-e1.investis.com/en/admin/content/brick?title=auto&type=All&langcode=All"
        except TimeoutException as e:
            xlogging(2, repr(e))
    else:
        raise SystemExit(
            "Incorrect parameter passed to clean_automation function, please pass either 'content' or 'components'")


def wait_for_ajax_processes(module='undefined', up_to_wait=10):
    """ A growing list of various ajax progress spinners, this is used to be a generic catch all for loading spinners
    :param: module: Used for logging to specify what is being waited for or what triggered the ajax process
    :return: void """
    ajax_timer = RecordTime(module)
    try:
        ajax_timer.start()
        xlogging(2, f"Waiting for various ajax processes to be completed for {module}", 'y')
        xlogging(2, "Checking for fullscreen ajax process to finish")
        wait_for_element_to_disappear(xpath_start('div', '@class', 'ajax-progress ajax-progress-fullscreen'), up_to_wait)
        xlogging(2, "Checking for regular ajax process to finish")
        wait_for_element_to_disappear(xpath_start('div', '@class', 'ajax-progress'), up_to_wait)
        xlogging(2, "Checking for alternative regular ajax process to finish")
        wait_for_element_to_disappear(xpath_start('div', '@class', 'ajax-progress ajax-progress-throbber'), up_to_wait)
        xlogging(2, "Checking for mini loader on add component from layout builder")
        wait_for_element_to_disappear(xpath_start('span', '@class', 'loader loader--inline icon icon-size-20') + xpath_chainer('child', 'span', '@class', 'svg'), up_to_wait)
        ajax_timer.stop()
    except TimeoutException:
        prtsc(f'{module}_took_too_long')
        raise TimeoutException(f'Ajax spinner for {module} took more than {up_to_wait}')


def configure_combobox(box_wrapper, drop_value, box_type='combobox', box_no=1, module='undefined'):
    """ Wrapper has to be in a div
    :param box_wrapper: value of data-drupal-selector
    :param drop_value: value of option being selected
    :param box_type: Some menus are of the role of 'listbox', there may be other types that you click to bring up a menu
    :param box_no: Some wrappers have multiple comboboxes, by default the first is chosen, but if you wanted the
    second or third: 'box_no=2' or 'box_no=3'
    :param module: Used for logging to specify what the combobox belongs to in plain english
    :return: void """
    xlogging(2, f"Clicking combobox number {box_no} in {box_wrapper} for {module}", 'y')
    web_wait(xpath_start('div', '@data-drupal-selector', box_wrapper) + xpath_chainer('descendant', 'span', '@role', box_type) + f'[{box_no}]').click()
    xlogging(2, f"Clicking {drop_value} in combobox", 'y')
    web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', drop_value)).click()


def custom_combobox(box_wrapper, drop_value, wrapper_tag, box_type='combobox', box_no=1, module='undefined'):
    """ Wrapper has to be in a div
    :param wrapper_tag:
    :param box_wrapper: value of data-drupal-selector
    :param drop_value: value of option being selected
    :param box_type: Some menus are of the role of 'listbox', there may be other types that you click to bring up a menu
    :param box_no: Some wrappers have multiple comboboxes, by default the first is chosen, but if you wanted the
    second or third: 'box_no=2' or 'box_no=3'
    :param module: Used for logging to specify what the combobox belongs to in plain english
    :return: void """
    xlogging(2, f"Clicking combobox number {box_no} in {box_wrapper} for {module}", 'y')
    web_wait(xpath_start(wrapper_tag, '@data-drupal-selector', box_wrapper) + xpath_chainer('descendant', 'span', '@role', box_type) + f'[{box_no}]').click()
    xlogging(2, f"Clicking {drop_value} in combobox", 'y')
    web_wait(xpath_start('span', '@class', 'select2-results') + xpath_chainer('descendant', 'li', 'text()', drop_value)).click()


def get_image(pic_num):
    """ Gets specific image from file browser
    :param pic_num: Which image to choose from config.yml
    :return: void """
    web_wait(xpath_start('a', '@class', 'imce-filefield-link')).click()
    xlogging(2, 'Switching to file browser window')
    window_switcher.SelectImage(file_browser_image_name(pic_num)).execute()
    xlogging(2, 'Switching back to main window')
    wait_for_ajax_processes(f'loading image {file_browser_image_name(pic_num)}')


def go_back():
    driver.forward()


def go_forward():
    driver.back()


def refresh_page():
    driver.refresh()


def clear_cookies():
    driver.delete_all_cookies()
