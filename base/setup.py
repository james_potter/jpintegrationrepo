# import additional python functionality
import logging
from pathlib import Path
import yaml
from selenium import webdriver
import sys
from drivers import relpath
import re

path = Path(__file__).parent / "../config/setup.yml"
with path.open() as f:  # Loads yaml config file to be referenced elsewhere in setup.py
    yml_config = yaml.load(f, Loader=yaml.FullLoader)


def argument_check(re_search, sys_argv_num, expected_arg, configpos1, configpos2):
    """ If a sys argument is not passed it will refer to the setup.yml, if setup.yml is invalid it will ask for user input.
    :param re_search: searched the string from args / setup.yml / user input
    :param sys_argv_num: Index of argument (1 = first position)
    :param expected_arg: Expected string to be passed
    :param configpos1: setup.yml parent (Example: fruit)
    :param configpos2: setup.yml child (Example: orange)
    :return: void """
    try:
        print(f"Argument {sys_argv_num} = {sys.argv[sys_argv_num]}")
    except IndexError:
        raise SystemExit(f"No value passed for argument {sys_argv_num}.")
    try:
        if not re.search(f'{re_search}', sys.argv[sys_argv_num]):
            print(f"Expected argument: {expected_arg} in position {sys_argv_num}")
            print("Checking yml configuration")
            sys.argv[sys_argv_num] = yml_config[configpos1][configpos2]
    except IndexError:
        print("Unable to get configuration from setup.yml")
        while not re.search(f'{re_search}', sys.argv[sys_argv_num]):
            sys.argv[sys_argv_num] = input(f"Please enter: {expected_arg}")
            if not re.search(f'{re_search}', sys.argv[sys_argv_num]):
                print("Incorrect value, please try again")


class DriverSetup(object):
    _instance = None
    driver = None
    options = None

    def __new__(cls):
        if cls._instance is None:
            print('Setting driver')
            cls._instance = super(DriverSetup, cls).__new__(cls)

        return cls._instance

    def __init__(self):
        if sys.argv[2] == 'Firefox' or 'Chrome' or 'Edge':
            self.driver = getattr(webdriver, sys.argv[2])(options=options, executable_path=relpath.get_full_driver_path(sys.argv[2], sys.argv[1]))
        else:
            raise SystemExit("Argument required in position 2: Browser - 'Chrome', 'Firefox', 'Linux'")

    def get_driver(self):
        return self.driver

    def set_driver(self, new_driver):
        self.driver = new_driver


class EnvironmentSetup:
    _instance = None
    base_url = None
    login_username = None
    login_password = None

    def __new__(cls):
        if cls._instance is None:
            print('Initializing setup.yml')
            cls._instance = super(EnvironmentSetup, cls).__new__(cls)

        return cls._instance

    def __init__(self):
        self.base_url = sys.argv[3]
        self.login_username = sys.argv[4]
        self.login_password = sys.argv[5]

    def get_base_url(self):
        return self.base_url

    def set_base_url(self, new_url):
        sys.argv[3] = new_url
        return self.base_url

    def get_member_username(self):
        return self.login_username

    def set_member_username(self, username):
        self.base_url = username

    def get_member_password(self):
        return self.login_password

    def set_member_password(self, password):
        self.base_url = password


def file_browser_image_name(pic_num):
    """ Returns the filename used when selecting an image from the file browser from the config file
    :return: config['running_config']['image_filename'] """
    return yml_config['running_config_images'][f'image_filename{pic_num}']


argument_check('win|mac|lin', 1, "'win', 'mac' or 'lin'", 'conf_os', 'os')
argument_check('Firefox|Chrome', 2, "'Firefox' or 'Chrome'", 'conf_driver', 'browser')
argument_check('http://|https://', 3, "'http://...com/' or 'https://...com/'", 'conf_url_base', 'url')
argument_check('.*', 4, "a value for username", 'credentials', 'username')
argument_check('.*', 5, "a value for password", 'credentials', 'password')

if sys.argv[2] == 'Firefox':
    from selenium.webdriver.firefox.options import Options
elif sys.argv[2] == 'Chrome':
    from selenium.webdriver.chrome.options import Options

options = Options()
options.headless = True
if sys.argv[2] == 'Chrome':
    options.add_argument("--window-size=1920,4000")

if sys.argv[2] == 'Firefox':
    options.add_argument("--width=1920")
    options.add_argument("--height=4000")

driver = DriverSetup().get_driver()
if not options.headless:
    driver.maximize_window()


login_username = EnvironmentSetup().get_member_username()
login_password = EnvironmentSetup().get_member_password()
driver.get(EnvironmentSetup().get_base_url())
base_url = EnvironmentSetup().get_base_url()


def initiate_logs_config():
    """ :return: logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(asctime)s - %(threadName)s%(message)s') """
    return logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(asctime)s:%(message)s')  # Can also add which thread: %(threadName)s or? %(thread)s | Add "filename='logs.txt'," arg to log to file