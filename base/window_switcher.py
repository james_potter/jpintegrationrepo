from selenium.common.exceptions import TimeoutException

from images.upload.get_image_path import get_image_path
from base.utils import *


class WindowSwitcher:
    def __init__(self):
        self.parent_handle = driver.current_window_handle

    def switch_to_new_window(self):
        handles = driver.window_handles
        xlogging(2, f"Current window IDs active: {handles}")
        size = len(handles)
        for i in range(size):
            xlogging(2, f"Parent window: {self.parent_handle}", 'y')
            xlogging(2, f"Checking window {i}", 'y')
            if handles[i] != self.parent_handle:
                xlogging(2, f"New window {i} found", 'y')
                driver.switch_to.window(handles[i])

    def operate_within_new_window(self, image_state='public'):
        pass

    def switch_to_parent_window(self):
        handles = driver.window_handles
        xlogging(2, f"Current window IDs active: {handles}")
        xlogging(2, "Checking for additional windows, other than parent window")
        size = len(handles)
        for i in range(size):
            xlogging(2, f"Checking window {i}", 'y')
            if handles[i] != self.parent_handle:
                driver.switch_to.window(handles[i])
                xlogging(4, f"Windows handles error, too many open windows. Closing window: {i}", 'y')
                driver.close()
            else:
                break
        xlogging(2, "Switching back to parent window")
        driver.switch_to.window(self.parent_handle)

    def execute(self):
        self.switch_to_new_window()
        self.operate_within_new_window()
        self.switch_to_parent_window()


class UploadImages(WindowSwitcher):
    def __init__(self, image_filename):
        driver.get(f'{EnvironmentSetup().get_base_url()}en/')
        web_wait(xpath_start('div', '@id', 'imce-toolbar-tab')).click()
        self.image_filename = image_filename
        super().__init__()

    def operate_within_new_window(self, image_state='public'):
        try:
            for image_name in self.image_filename:
                web_wait(xpath_start('span', '@role', 'button') + xpath_chainer('child', 'span', 'text()', 'Upload')).click()
                if image_state == 'public':
                    web_wait(xpath_start('input', '@class', 'imce-uq-input')).send_keys(
                        get_image_path(image_name))
                elif image_state == 'protected':
                    web_wait(xpath_start('input', '@class', 'imce-uq-protected-input')).send_keys(
                        get_image_path(image_name))
                else:
                    raise Exception("Please pass 'public' or 'protected' when calling 'upload_image'")
                wait_for_element_to_disappear(xpath_start('div', ' @class', 'imce-uqi active'))
        except TimeoutException as e:
            xlogging(2, repr(e), 'y')
            xlogging(2, f"Something when wrong when trying to upload images, taking a screenshot", 'y')
            prtsc('Unable to upload image')


class SelectImage(WindowSwitcher):
    def __init__(self, image_name):
        self.image_name = image_name
        super().__init__()

    def operate_within_new_window(self, image_state='public'):
        try:
            try:
                xlogging(2, f"Selecting image: {self.image_name}", 'y')
                web_wait(xpath_start("div", "@title", self.image_name)).click()
            except TimeoutException as e:
                xlogging(2, repr(e))
                xlogging(2, f"Xpath failed - selecting image: {self.image_name} (protected) instead", 'y')
                web_wait(xpath_start("div", "@title", self.image_name + " (Protected)")).click()
        except TimeoutException as e:
            xlogging(2, repr(e), 'y')
            xlogging(2, "Something when wrong when trying to select an image, taking a screenshot", 'y')
            prtsc('Unable to select image')
        xlogging(2, "Clicking select", 'y')
        web_wait(xpath_start("div", "@id", "imce-toolbar") + xpath_chainer("child", "span", "@class", "imce-tbb imce-ficon imce-ficon-check imce-tbb--sendto")).click()
