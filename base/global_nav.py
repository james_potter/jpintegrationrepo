""" Contains all navigation related functionality """
import glob
from .tools import *
from .utils import *
from base import window_switcher


def add_content_page(content_name, url_suffix, date_time):
    """ Goes to content page
    :param date_time: String to be passed into logs and into tile field.
    :param content_name: Puts the whatever is passed down from content_name into text boxes etc. example: Banner
    :param url_suffix: The string required after .com/en/node/add/ <url_suffix> example:bricky
    :return: void """
    xlogging(2, f"Adding content page {content_name} by going to /en/node/add/{url_suffix}", 'y')
    driver.get(f"{EnvironmentSetup().get_base_url()}en/node/add/{url_suffix}")
    xlogging(1, "Typing: " + date_time + " " + content_name + ": into title field", 'y')
    web_wait("(" + xpath_start("input", "@class", "js-text-full text-full form-text required") + ")[1]").send_keys(date_time + " " + content_name)


def set_content_page_url_alias(url_suffix):  # TODO: Clean this one up and possibly send to class, this will remove try expect for specific content types
    """ Goes to content page
    :param url_suffix: Has a different use from 'add_content_page' as this is used when
    generating URL alias for content pages
    :return: void """
    xlogging(2, "Clicking url Alias menu item", 'y')
    web_wait(xpath_start('details', '@data-drupal-selector', 'edit-path-0')).click()
    xlogging(2, f"url_suffix = {url_suffix}", 'y')
    try:
        if web_wait(xpath_start('input', '@data-drupal-selector', 'edit-path-0-pathauto'), 1).get_attribute('checked') == 'true':
            xlogging(2, "Generate automatic URL alias is checked, unchecking", 'y')
            web_wait(xpath_start('input', '@data-drupal-selector', 'edit-path-0-pathauto')).click()
    except TimeoutException as e:
        xlogging(3, f"Unable to find check box for URL alias, error: {repr(e)}", 'y')
    xlogging(2, f"Typing {url_suffix} into url alias text box", 'y')
    web_wait(xpath_start('input', '@data-drupal-selector', 'edit-path-0-alias')).send_keys(url_suffix)


def set_content_release_state(content_state):
    """ Sets the release state of a content page, such as a bricky in the status of draft, review or published.
    :param content_state: Sets the release state of the content page, use: 'Draft', 'Review' or 'Published'
    :return: void """
    try:
        configure_combobox('edit-moderation-state-wrapper', content_state)
    except TimeoutException as e:
        xlogging(2, repr(e), 'y')
        reset_step_counter('fail', "Able to select drop down", 'Unable to select drop down')


def save_content_page(content_name):
    """ Mostly for logging purposes to use the name of the content page being created
    :param content_name: Name of content page (although, could be any string)
    :return: void """
    wait_for_ajax_processes(f'before clicking save for {content_name}')
    xlogging(2, f'Clicking save for {content_name}', 'y')
    web_wait(xpath_start('div', '@data-drupal-selector', 'edit-actions') + xpath_chainer('child', "input", "@class", "button button--primary js-form-submit form-submit")).click()


def edit_recent_content_page(row_number):
    driver.get(f"{EnvironmentSetup().get_base_url()}en/admin/content")
    web_wait(By.CSS_SELECTOR, ".views-table > tbody:nth-child(2) > tr:nth-child(" + str(row_number) + ") > td:nth-child(7) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)").click()


def layout_of_recent_bricky(row_number):
    xlogging(2, "Going to recent content page", 'y')
    driver.get(f"{EnvironmentSetup().get_base_url()}en/admin/content")
    web_wait(By.CSS_SELECTOR, ".views-table > tbody:nth-child(2) > tr:nth-child(" + str(row_number) + ") > td:nth-child(7) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)").click()
    web_wait(By.CSS_SELECTOR, "li.tabs__tab:nth-child(5) > a:nth-child(1)").click()


def save_bricky_layout(page_name):
    xlogging(2, "Waiting for background overlay to disappear")
    WebDriverWait(driver, 10).until(ec.invisibility_of_element_located((By.XPATH, "//div[@class='ui-widget-overlay ui-front']")))
    xlogging(2, "Saving " + page_name + " layout")
    web_wait(xpath_start("div", "@class", "col-xs-12") + xpath_chainer("child", "div", "@class", "form-actions js-form-wrapper form-wrapper") + xpath_chainer("child", "input", "@class", "button button--primary js-form-submit form-submit form-control")).click()


def user_login():
    """ Go to login page and enter credentials provided in system arguments
    :return: void """
    driver.get(f"{EnvironmentSetup().get_base_url()}en/member/login")
    xlogging(1, "Sending username keys to name field")
    web_wait(xpath_start('input', '@data-drupal-selector', 'edit-name')).send_keys(login_username)
    xlogging(1, "Sending password keys to pass field")
    web_wait(xpath_start('input', '@data-drupal-selector', 'edit-pass')).send_keys(login_password)
    xlogging(1, "Submitting username and password form")
    web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit')).click()
    xlogging(2, "Closing cookie policy")
    web_wait(xpath_start('a', '@id', 'cookie-agree')).click()


def save_layout():
    xlogging(3, "save_layout function is not available due to an issue with trying to scroll the layout builder save button into view")


def environment_setup():
    initiate_logs_config()

    files = glob.glob(r'images\*.png')
    for f in files:
        xlogging(2, f"Removing image: {f}")
        os.remove(f)

    with open('steps.txt', 'w') as f:
        f.write("Starting Run...\n")

    with open('results.txt', 'w') as f:
        f.write("Starting Run...\n")

    initiate_logs_config()

    xlogging(2, "Starting program")

    user_login()
    xlogging(2, "UPLOADING TEST IMAGES")
    images = []
    for i in glob.glob(r'images\upload\*.jpg'):
        i = i.replace('images\\upload\\', '')
        images.append(i)
    print(images)
    window_switcher.UploadImages(images).execute()

    clean_automation('content')
    clean_automation('components')
