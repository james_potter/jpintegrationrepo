import os
import time
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from .setup import *

stepNo = 0


class RecordTime:
    def __init__(self, benched_module):
        """ RecordTime:
        Use: .start to start the timer
        Use: .stop to stop the timer and log time taken
        :param benched_module: lower case string to identify code block execution time for logging purposes """
        self.start_time = None
        self.stop_time = None
        self.benched_module = benched_module

    def start(self):
        """ Gets an instance of datetime
        Put this before an action - to be used with stop_timer function
        :return: void """
        self.start_time = time.perf_counter()
        xlogging(2, f'Starting timer for {self.benched_module} at {round(self.start_time, 2)} into test')

    def calc(self):
        """ Used to inform the user via the console how long a specific action took
        :return: Logging_With_Global_Wait(2, f'{benchedModule} finished in {round(stopTimer - startTimer, 2)} second(s)') """
        return f'{self.benched_module} finished in {round(self.stop_time, 2) - round(self.start_time, 2)} second(s)'

    def stop(self):
        """ Gets an instance of datetime
        Put this after an action - to be used with start_timer function
        :return: void """
        self.stop_time = time.perf_counter()
        xlogging(2, f'Stopping timer for {self.benched_module} at {round(self.stop_time, 2)} into test')
        xlogging(2, self.calc(), 'y')


def xlogging(set_debug_level, text_out, log_as_step='n', sleep_secs=0):
    """ This allows a specific wait time to be passed when sending a message to the console
    :param log_as_step: default is 'n', use count_step='y' if log should also be counted as a step to be logged to steps file
    :param set_debug_level: 1:DEBUG, 2:INFO, 3:, WARNING, 4:ERROR, 5:CRITICAL
    :param text_out: Message to be printed to the console
    :param sleep_secs: How long to wait before continuing with the program
    :return: Void """
    if set_debug_level == 1:
        debug_level = logging.debug
    elif set_debug_level == 2:
        debug_level = logging.info
    elif set_debug_level == 3:
        debug_level = logging.warning
    elif set_debug_level == 4:
        debug_level = logging.error
    elif set_debug_level == 5:
        debug_level = logging.critical
    else:
        debug_level = 2

    if log_as_step == 'n':
        if sleep_secs != 0:
            debug_level(f' {text_out}; wait duration: {sleep_secs} second(s)')
        else:
            debug_level(f' {text_out}')
        time.sleep(sleep_secs)

    if log_as_step == 'y':
        if sleep_secs != 0:
            debug_level(f' {text_out}; wait duration: {sleep_secs} second(s)')
            step_counter(text_out, sleep_secs)
        else:
            debug_level(f' {text_out}')
            step_counter(text_out, sleep_secs)
        time.sleep(sleep_secs)


def new_date_time():
    current_date_time = datetime.now()
    date_time = ("AUTO: " + current_date_time.strftime("%d/%m/%Y %H_%M_%S"))  # Get current date and format date time
    return date_time


def xpath_start(tag, attribute, value):
    """ Allows quick templating of basic Xpaths or the start of longer ones
    example: //tag[@attribute='attribute value']
    :param tag: Examples: div, a, li, ul, Span, OtherAnything
    :param attribute: Examples: class@, class(), text(), @anything
    :param value: Value of attribute (after the =)
    :return: f"//{tag}[{attribute}='{value}']" """
    return f"//{tag}[{attribute}='{value}']"


def xpath_chainer(relative, tag, attribute, value):
    """ Allows quick templating of chained potions of xpaths
    example: /relative::tag[@attribute='attribute value']
    :param relative: Are you looking for a parent, child, ancestor, descendant or sibling etc?
    :param tag: Examples: <div>, <a>, <li>, <ul>, <Span>, <OtherAnything>
    :param attribute: Examples: class@, class(), text(), @anything
    :param value: Value of attribute (after the =)
    :return: f"//{tag}[{attribute}='{value}']" """
    return f"/{relative}::{tag}[{attribute}='{value}']"


def web_wait(value, wait_time=5):
    """ Wait for element to APPEAR up to a GLOBALLY defined value, in this case: "WebDriverWait(5)" - 5 = 5 seconds
    5 seconds being the maximum amount of time selenium will wait before erroring.
    :param field: Are you going to find your element: Type "By." followed by: CLASS, CSS_SELECTOR or NAME etc.
    :param value: The path or value of the type of field you are looking to use as reference
    :param wait_time: An 'up to' value for time to wait for element to be available, default is 5(seconds), but can be specified to be shorter or longer """
    if wait_time != 5:  # IF CHANGED: Ensure this value matches default value for wait_time
        xlogging(2, f"Waiting up to: {wait_time} second(s) to locate a single element's xpath, with a value of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tWaiting up to: {wait_time} second(s) to locate a single element's xpath, with a value of: {value}\n")
    else:
        xlogging(2, f"Locating a single element's xpath with a value of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tLocating a single element's xpath with a value of: {value}\n")

    return WebDriverWait(driver, wait_time).until(ec.presence_of_element_located((By.XPATH, value)))


def get_elements(field, value, wait_time=5):
    if wait_time != 5:  # IF CHANGED: Ensure this value matches default value for wait_time
        xlogging(2, f"Waiting up to: {wait_time} second(s) to locate elements using xpath, with a value of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tWaiting up to: {wait_time} second(s) to locate elements using xpath, with a value of: {value}\n")
    else:
        xlogging(2, f"Locating elements using xpath, with a value of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tLocating elements using xpath, with a value of: {value}\n")

    return WebDriverWait(driver, wait_time).until(ec.presence_of_all_elements_located((field, value)))


def wait_for_element_to_disappear(value, wait_time=5):
    """ Wait for element to DISAPPEAR up to a GLOBALLY defined value, unless specified when calling the function
    For example: A 5 second wait_time being the maximum amount of time selenium will wait before erroring.
    :param wait_time: Optional amount of time to wait
    :param field: Are you going to find your element "By." CLASS, CSS_SELECTOR or NAME etc.
    :param value: The path or value of the type of field you are looking to use as reference
    :return: WebDriverWait(5).until(ec.presence_of_element_located((field, path))) """
    if wait_time != 5:  # IF CHANGED: Ensure this value matches default value for wait_time
        xlogging(2, f"Custom up-to wait time of: {wait_time} second(s) for element to disappear for xpath with a path of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tWaiting up to: {wait_time} second(s) for element to disappear for xpath, with a value of: {value}\n")
    else:
        xlogging(2, f"Waiting for element to disappear for xpath with a value of: {value}")
        with open('steps.txt', 'a') as file:
            file.write(f"\tWaiting for element to disappear for xpath with a value of: {value}\n")

    return WebDriverWait(driver, wait_time).until(ec.invisibility_of_element_located((By.XPATH, value)))


def prtsc(pic_filename):
    """ Take a screenshot of the active window
    :param pic_filename: String to be used as the file path ./ can be used if screen shots are to be dumped in the root of the project folder
    :return: void """
    try:
        os.mkdir('images')
        xlogging(2, f"Directory: 'images' created")
    except FileExistsError:
        xlogging(1, f"Directory: 'images' already exists, ignoring")
    xlogging(2, f"Saving screenshot {pic_filename} to ./images", 'y')
    driver.save_screenshot(f"./images/{pic_filename}.png")
    xlogging(2, pic_filename + ": saved")


def handle_file_browser_window(img_name):
    """
    Switches to file browser popup window (1)
    Selects the image
    Switches back to the original window (0)
    :param img_name: Typically imported from the config.yml file under: running_config>image_filename to be used to select from the list of files in the file browser
    :return: void """
    handles = driver.window_handles
    xlogging(2, f"Current window IDs active: {handles}")
    size = len(handles)
    parent_handle = driver.current_window_handle
    for i in range(size):
        xlogging(2, f"current win loop: {i}")
        if handles[i] != parent_handle:
            driver.switch_to.window(handles[i])

    handles = driver.window_handles
    xlogging(2, f"Current window IDs active: {handles}")
    size = len(handles)

    xlogging(2, "Checking for additional windows, other than parent window")
    for i in range(size):
        xlogging(2, f"current win loop: {i}")
        if handles[i] != parent_handle:
            driver.switch_to.window(handles[i])
            xlogging(4, f"Windows handles error, too many open windows. Closing window: {i}", 'y')
            driver.close()
    xlogging(2, "Switching back to parent window")
    driver.switch_to.window(parent_handle)


def random_hex_colour(selector_value):
    """ Generates random hex value for colours example: '#ffffff', '#1D4ADD'
    :param selector_value: A string to pass into an xpath query to send keys to an input tag with a attribute of 'data-drupal-selector'
    :return: void """
    import random
    r = lambda: random.randint(0, 255)
    colour = '#{:02x}{:02x}{:02x}'.format(r(), r(), r())
    web_wait(xpath_start('input', '@data-drupal-selector', selector_value)).send_keys(colour)


def teardown():
    """ Closes driver instance
    :return: void """

    driver.close()
    driver.quit()


def start_step_counter(test_title):
    """
    :param test_title: Title of the test
    :return: void """

    reset_step_counter('none')
    with open('steps.txt', 'a') as f_start:
        f_start.write(
            f'''

{datetime.now().strftime("%d/%m/%Y %H_%M_%S")}
Test: {test_title}
Steps:
''')


def step_counter(text_out, sleep_secs):
    global stepNo
    # global isStepCounterReset
    stepNo += 1

    with open('steps.txt', 'a') as f_step:
        if sleep_secs != 0:
            f_step.write(f"{str(stepNo).zfill(2)}) {text_out}; wait duration: {sleep_secs} second(s).\n")
        f_step.write(f"{str(stepNo).zfill(2)}) {text_out}.\n")


def reset_step_counter(pass_fail, expected_result='', actual_result=''):
    global stepNo
    # global isStepCounterReset
    if pass_fail == 'none':
        xlogging(2, "No pass / fail value passed, resetting step counter")

    if pass_fail == 'pass':
        with open('steps.txt', 'a') as f_pass:
            f_pass.write('Test Passed')

    if pass_fail == 'fail':
        with open('steps.txt', 'a') as f_fail:
            f_fail.write('Test failed:\n')
            f_fail.write(f"Expected Result:\n{expected_result}\n")
            f_fail.write(f"\nActual Result:\n{actual_result}")
    stepNo = 0
