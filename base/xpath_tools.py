""" quick creation of common types of xpaths """
from base.utils import *


class XpathTool:
    def __init__(self, xstart_tag, xstart_attribute, xstart_value, xchain_relative='', xchain_tag='', xchain_attribute='', xchain_value='', wait_time=5):
        self.xstart_tag = xstart_tag
        self.xstart_attribute = xstart_attribute
        self.xstart_value = xstart_value
        self.xchain_relative = xchain_relative
        self.xchain_tag = xchain_tag
        self.xchain_attribute = xchain_attribute
        self.xchain_value = xchain_value
        self.wait_time = wait_time

    def xpath_starter(self, tag, attribute, value):
        """ Allows quick templating of basic Xpaths or the start of longer ones
        example: //tag[@attribute='attribute value']
        :param tag: Examples: div, a, li, ul, Span, OtherAnything
        :param attribute: Examples: class@, class(), text(), @anything
        :param value: Value of attribute (after the =)
        :return: f"//{tag}[{attribute}='{value}']" """
        return f"//{tag}[{attribute}='{value}']"

    def xpath_chainer(self, relative, tag, attribute, value):
        """ Allows quick templating of chained potions of xpaths
        example: /relative::tag[@attribute='attribute value']
        :param relative: Are you looking for a parent, child, ancestor, descendant or sibling etc?
        :param tag: Examples: <div>, <a>, <li>, <ul>, <Span>, <OtherAnything>
        :param attribute: Examples: class@, class(), text(), @anything
        :param value: Value of attribute (after the =)
        :return: f"//{tag}[{attribute}='{value}']" """
        return f"/{relative}::{tag}[{attribute}='{value}']"

    def xpath_start(self):
        """ call this to only find by simple xpath i.e. //tag[attribute='value']
        :return: """
        if self.wait_time != 5:  # IF CHANGED: Ensure this value matches default value for wait_time
            xlogging(2, f"Custom up-to wait time of: {self.wait_time} second(s) for field: {By.XPATH} with a path of: {self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value)}")
        else:
            xlogging(2, f"Using field: {By.XPATH} with a path of: {self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value)}")
        # xlogging(2, self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value))
        return WebDriverWait(driver, self.wait_time).until(ec.presence_of_element_located((By.XPATH, self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value))))

    def xpath_chain(self):
        if self.wait_time != 5:  # IF CHANGED: Ensure this value matches default value for wait_time
            xlogging(2, f"Custom up-to wait time of: {self.wait_time} second(s) for field: {By.XPATH} with a path of: {path}")
        else:
            xlogging(2, f"Using field: {By.XPATH} with a path of: {path}")
        # xlogging(2, self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value) + self.xpath_chainer(self.xchain_relative, self.xchain_tag, self.xchain_attribute, self.xchain_value))
        return WebDriverWait(driver, self.wait_time).until(ec.presence_of_element_located((By.XPATH, self.xpath_starter(self.xstart_tag, self.xstart_attribute, self.xstart_value)
                                                                                           + self.xpath_chainer(self.xchain_relative, self.xchain_tag, self.xchain_attribute, self.xchain_value))))


class DivDDS(XpathTool):
    def __init__(self, xstart_value, xstart_tag='div', xstart_attribute='@data-drupal-selector', xchain_relative='', xchain_tag='', xchain_attribute='', xchain_value='', wait_time=5):
        super().__init__(xstart_tag, xstart_attribute, xstart_value, xchain_relative, xchain_tag, xchain_attribute, xchain_value, wait_time)


class InputDDS(XpathTool):
    def __init__(self, xstart_value, xstart_tag='input', xstart_attribute='@data-drupal-selector', xchain_relative='', xchain_tag='', xchain_attribute='', xchain_value='', wait_time=5):
        super().__init__(xstart_tag, xstart_attribute, xstart_value, xchain_relative, xchain_tag, xchain_attribute, xchain_value, wait_time)
