""" Adds the content page bricky with the status of Draft """
from base.tools import *
from selenium.webdriver.common.action_chains import ActionChains
from base.xpath_tools import *


class ComponentsBase:
    def __init__(self, component_upper_underscore, component_label_value, date_time):
        self.component_upper_underscore = component_upper_underscore
        self.component_label_value = component_label_value
        self.date_time = date_time

    @staticmethod
    def edit_component(component):
        """
        :param component: Uses the component label name from the drop down or from the side menu from layout
        :return: void """
        xlogging(2, f"Attempting to click edit button for {component}")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-entities-0-actions-ief-entity-edit')).click()

    def edit_component_name(self, component):
        """ Types into the mandatory field for component title
        :param component: Uses the component label name from the drop down or from the side menu from layout
        :return: void """
        xlogging(2, f"Typing {self.date_time} {component} into component title.", 'y')
        new_name = new_date_time()
        try:
            xlogging(2, "Attempting initial edit of component title")
            wait_for_ajax_processes()
            web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-title-0-value'), 1).send_keys(f"{self.date_time} {component}")
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(2, "Could not find component title: Are you editing? Trying alternative path for inline editing")
            web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-title-0-value'), 3).send_keys(f"{new_name} {component}")

    def save_component(self):
        """ Clicks the add component button
        :return: void """
        xlogging(2, f"Clicking create component for {self.component_label_value}", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-actions-ief-add-save')).click()
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-entities-0-actions-ief-entity-edit'))
        wait_for_ajax_processes()

    @staticmethod
    def open_pencil_menu():
        """ Hovers the body of the bricky page to show the pencil icon then clicks it.
        :return: void """

        web_wait(xpath_start('div', '@class', 'layout-flexible container'))

        # layoutPath = web_wait(xpath_start('div', '@class', 'layout-flexible container'))
        layout_path = web_wait(xpath_start('div', '@class', 'layout-flexible container') + '/child::div')
        try:
            xlogging(2, "Attempting to hover", 3)
            ActionChains(driver).move_to_element(layout_path).perform()
            xlogging(2, "Finished hovering", 1)
            xlogging(2, "Clicking pencil menu")
            web_wait(xpath_start('button', '@class', 'trigger focusable')).click()
        except TimeoutException as e:
            xlogging(2, repr(e))
            xlogging(2, "Second try: Attempting to hover", 1)
            ActionChains(driver).move_to_element(layout_path).perform()
            xlogging(2, "Second try: Finished hovering", 1)
            xlogging(2, "Second try: Clicking pencil menu")
            web_wait(xpath_start('button', '@class', 'trigger focusable')).click()

    def bricky_select_component(self):
        """ Clicks the drop down menu for the body in the advanced edit page, selects the 'component' and
        clicks add component
        :return: void """
        configure_combobox('edit-field-body-wrapper', self.component_label_value)
        xlogging(2, "Clicking Add new component for 'Body'", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-actions-ief-add')).click()
        self.edit_component_name(self.component_label_value)

    def go_to_advanced_edit(self):
        """ This function is separate from 'open_pencil_menu' because a param can be added to select anything from the
        pencil menu
        :return: void """

        self.open_pencil_menu()
        xlogging(2, "Clicking Advanced edit")
        web_wait(xpath_start('div', '@class', 'contextual open') + xpath_chainer('descendant', 'a', 'text()', 'Advanced edit')).click()

    def change_component_name(self):
        """ Renames the component to a new value
        :return: void """
        self.edit_component(self.component_upper_underscore)
        xlogging(2, "Clearing component title")
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-title-0-value')).clear()
        self.edit_component_name(self.component_upper_underscore)

    def change_component_state(self, component):
        self.edit_component(component)
        component_state = web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-moderation-state-wrapper') + xpath_chainer('descendant', 'span', '@role', 'textbox')).text
        xlogging(2, f"Value of dropdown / componentState: {component_state}")
        if component_state == 'Draft':
            web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-moderation-state-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
            web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-moderation-state-wrapper') + xpath_chainer('descendant', 'li', 'text()', 'Published')).click()
        elif component_state == 'Published':
            web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-moderation-state-wrapper') + xpath_chainer('descendant', 'span', '@role', 'combobox')).click()
            web_wait(xpath_start('div', '@data-drupal-selector', 'edit-field-body-form-inline-entity-form-entities-0-form-moderation-state-wrapper') + xpath_chainer('descendant', 'li', 'text()', 'Draft')).click()
        else:
            xlogging(4, "Text has no value in>moderation state>combobox of the component")

    @staticmethod
    def toggle_component_shared_status():
        xlogging(2, "Clicking share checkbox", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-field-body-form-0-shared-status-value')).click()

    def assert_page_name(self):
        page_name_assertion = web_wait(xpath_start('div', '@class', 'col-xs-12') + "/descendant::h1").text
        xlogging(2, f"Asserting if page title = {self.date_time} {self.component_label_value}")
        xlogging(2, page_name_assertion)
        xlogging(2, f"{self.date_time} {self.component_label_value}")
        assert page_name_assertion == f"{self.date_time} {self.component_label_value}"
        xlogging(2, f"Assertion passed")

    def go_to_edit_layout_of_created_page(self):
        xlogging(2, f"Going to '{EnvironmentSetup().get_base_url()}en/admin/content'", 'y')
        driver.get(f'{EnvironmentSetup().get_base_url()}en/admin/content')
        web_wait(xpath_start('div', '@class', 'view-filters') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-title')).send_keys(self.date_time)
        web_wait(xpath_start('div', '@class', 'view-filters') + xpath_chainer('descendant', 'input', '@data-drupal-selector', 'edit-submit-content')).click()
        web_wait(xpath_start('li', '@class', 'edit dropbutton-action') + xpath_chainer('descendant', 'a', 'text()', 'Edit')).click()
        web_wait(xpath_start('ul', '@class', 'tabs primary clearfix') + xpath_chainer('descendant', 'a', 'text()', 'Layout')).click()

    def add_existing_component_in_layout_builder(self):
        xlogging(2, f"Clicking add component under: {self.component_label_value} component in layout builder", 'y')
        web_wait(xpath_start('div', '@class', 'row') + xpath_chainer('descendant', 'a', 'text()', 'Add component ')).click()
        wait_for_ajax_processes()
        xlogging(2, "Clicking 'Select existing'", 'y')
        web_wait(xpath_start('div', '@id', 'drupal-off-canvas') + xpath_chainer('descendant', 'a', 'text()', 'Select existing')).click()
        xlogging(2, f"Typing {self.date_time} {self.component_label_value} into search box", 'y')
        web_wait(xpath_start('input', '@placeholder', 'Filter by component name')).send_keys(f'{self.date_time} {self.component_label_value}')
        xlogging(2, f"Clicking component with the name: {self.date_time} {self.component_label_value}", 'y')
        web_wait(xpath_start('ul', '@class', 'links') + xpath_chainer('descendant', 'a', 'text()', f'{self.date_time} {self.component_label_value}')).click()

    def add_new_component_from_layout_builder(self):
        web_wait(xpath_start('div', '@class', 'row') + xpath_chainer('descendant', 'a', 'text()', 'Add component ')).click()
        web_wait(xpath_start('div', '@class', 'form-item__control textfield') + xpath_chainer('child', 'input', '@type', 'search')).send_keys(self.component_label_value)
        web_wait(xpath_start('ul', '@class', 'links') + xpath_chainer('descendant', 'a', 'text()', self.component_label_value)).click()
        wait_for_ajax_processes(f'Loading modal for {self.component_upper_underscore}')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-title-0-value')).send_keys(f'{self.date_time} {self.component_label_value}')

    def filter_component_by_title(self):
        xlogging(2, f"Going to url: {EnvironmentSetup().get_base_url()}en/admin/content/components", 'y')
        driver.get(f"{EnvironmentSetup().get_base_url()}/en/admin/content/components")
        xlogging(2, f"Typing '{self.date_time} {self.component_label_value}' into 'Title' search box", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-title')).send_keys(f"{self.date_time} {self.component_label_value}")
        xlogging(2, f"Clicking {str(web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit-components-list')).get_attribute('value'))} button", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit-components-list')).click()
        xlogging(2, f"Checking that there is a title with the value '{self.date_time} {self.component_label_value}' after filtering", 'y')
        web_wait(xpath_start('a', 'text()', f"{self.date_time} {self.component_label_value}"))

    def filter_component_component_type(self):
        custom_combobox('views-exposed-form-components-list-list', self.component_label_value, 'form', module='Component type')
        xlogging(2, f"Clicking {str(web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit-components-list')).get_attribute('value'))} button", 'y')
        web_wait(xpath_start('input', '@data-drupal-selector', 'edit-submit-components-list')).click()
        xlogging(2, f"Checking that there is a title with the value '{self.date_time} {self.component_label_value}' after filtering", 'y')
        web_wait(xpath_start('a', 'text()', f"{self.date_time} {self.component_label_value}"))

    def select_usage_counter(self, usage_assertion):
        self.filter_component_by_title()
        if usage_assertion != 0:
            xlogging(2, f"Asserting that usage counter = {usage_assertion}", 'y')
            assert web_wait(f"({xpath_start('td', '@headers', 'view-entity-usage-count-table-column')}{xpath_chainer('child', 'a', '@data-dialog-type', 'modal')})[1]").text == usage_assertion
            xlogging(2, f"Clicking usage counter for {self.component_label_value}", 'y')
            web_wait(f"({xpath_start('td', '@headers', 'view-entity-usage-count-table-column')}{xpath_chainer('child', 'a', '@data-dialog-type', 'modal')})[1]").click()
            odd_rows = len(WebDriverWait(driver, 5).until(ec.visibility_of_all_elements_located((By.XPATH, xpath_start('div', '@id', 'drupal-modal') + xpath_chainer('descendant', 'tr', '@class', 'odd')))))
            try:
                even_rows = len(WebDriverWait(driver, 1).until(ec.visibility_of_all_elements_located((By.XPATH, xpath_start('div', '@id', 'drupal-modal') + xpath_chainer('descendant', 'tr', '@class', 'even')))))
            except TimeoutException:
                even_rows = 0
            xlogging(2, odd_rows + even_rows)
            odd_even_table_rows = odd_rows + even_rows
            assert str(odd_even_table_rows) == str(usage_assertion)
            web_wait(xpath_start('button', '@title', 'Close')).click()
        else:
            assert web_wait(f"({xpath_start('td', '@headers', 'view-entity-usage-count-table-column')}{xpath_chainer('child', 'a', '@data-dialog-type', 'modal')})[1]").text == usage_assertion
            web_wait(f"({xpath_start('td', '@headers', f'{self.date_time} {self.component_label_value}')}))[1]").click()
