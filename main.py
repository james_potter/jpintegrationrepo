""" Runs setup before calling the test suite runner then ends the selenium instance """
import runner
from base.global_nav import *
from base.tools import *
import os

# TODO: (med) Document start and finish setup process for running this program
# TODO: (large) see if threading is possible with a new+separate class call for driver
# TODO: (large) add Pallav's component checks for anonymous user
# TODO: (very large) dive deeper into components to expand on functionality
# TODO: (very important) Get coffee
# blocked TODO: (med-very large) add a set of tests for creating components from layout builder
# blocked TODO: (large) add a pilot function for drag and drop
# TODO: (small) go to components hub and print the list of component titles
# TODO: (very large) second try at an element finder class

environment_setup()

whole_script = RecordTime("all tests")
whole_script.start()

base_path = fr"{os.path.join(os.path.dirname(__file__))}\test_suites"

try:
    print(sys.argv[6])
    system_arg6 = True
except IndexError:
    system_arg6 = False
if system_arg6:
    for i in sys.argv[6].split(','):
        runner.TestRunner(fr"{base_path}{i}").run()
if not system_arg6:
    xlogging(4, "No path argument passed in 6th position, running all tests")
    runner.TestRunner(fr"{base_path}\smoke\*\*\test*").run()
whole_script.stop()

driver.close()
driver.quit()
quit()

xlogging(2, 'finished...')
